#[macro_use]
extern crate log;

use std::{
  ffi::CString,
};

use bytes::Bytes;
use bytesize::ByteSize;
use indymilter::{
  Actions,
  Callbacks,
  Context,
  ContextActions,
  EomContext,
  NegotiateContext,
  ProtoOpts,
  Status,
};

use tokio::{
  net::TcpListener,
  signal,
};

use uwuifier::{
  uwuify_sse,
  round_up16,
};
use mailparse::{
  parse_mail,
  addrparse,
  ParsedMail,
  MailParseError,
  MailAddr,
  MailHeaderMap,
};
use lol_html::{
  text,
  HtmlRewriter,
  Settings,
};
use lol_html::html_content::ContentType;

struct EmailData {
  body: Vec<Bytes>,
  content_type: Option<String>,
}

#[tokio::main]
async fn main() {
  if env_logger::try_init().is_err() {
    eprintln!("Error initializing logger.");
  }

  info!("Starting up milter service...");

  let listener = TcpListener::bind("localhost:3000").await;
  match listener {
    Ok(listener) => {
      info!("Listener successfully bound to localhost:3000");

      let callbacks: Callbacks<EmailData> = Callbacks::new()
        .on_negotiate(|cx, actions, opts| Box::pin(handle_negotiate(cx, actions, opts)))
        .on_header(|cx, name, value| Box::pin(handle_header(cx, name, value)))
        .on_body(|cx, chunk| Box::pin(handle_body(cx, chunk)))
        .on_eom(|cx| Box::pin(handle_eom(cx)));

      let config = Default::default();

      info!("Running milter with set configurations and callbacks...");
      indymilter::run(listener, callbacks, config, signal::ctrl_c())
        .await
        .expect("milter execution failed");
      info!("Milter service stopped.");
    },
    Err(e) => {
      error!("Failed to bind listener to localhost:3000 due to: {}", e);
    }
  }
}

async fn handle_negotiate(
  context: &mut NegotiateContext<EmailData>,
  _: Actions,
  _: ProtoOpts,
) -> Status {
  debug!("Negotiating with the client...");
  context.requested_actions |= Actions::ADD_HEADER | Actions::REPLACE_BODY;

  debug!("Requested actions set to ADD_HEADER and REPLACE_BODY.");

  Status::Continue
}

async fn handle_header(context: &mut Context<EmailData>, name: CString, value: CString) -> Status {
  let name = name.to_str().unwrap();
  let value = value.to_str().unwrap();
  debug!("Processing header - {}: {}", name, value);

  if name.to_lowercase() == "subject" {
    return if value.to_lowercase().ends_with("~nyah") {
      debug!("Subject ends with ~nyah. Continuing processing.");
      Status::Continue
    } else {
      debug!("Subject does not end with ~nyah. Accepting the email as is.");
      Status::Accept
    }
  }

  if name.to_lowercase() == "from" {
    let parsed_addresses = match addrparse(value) {
      Ok(addresses) => addresses,
      Err(_) => {
        debug!("Error parsing 'from' address. Accepting the email as is.");
        return Status::Accept
      },
    };

    for address in parsed_addresses.iter() {
      match address {
        MailAddr::Single(single_info) => {
          if single_info.addr.ends_with("@nyah.cat") {
            debug!("Single address ends with @nyah.cat. Continuing processing.");
            return Status::Continue;
          }
        },
        MailAddr::Group(group_info) => {
          for single_info in &group_info.addrs {
            if single_info.addr.ends_with("@nyah.cat") {
              debug!("Group address contains an email ending with @nyah.cat. Continuing processing.");
              return Status::Continue;
            }
          }
        }
      }
    }

    debug!("No address matched the criteria. Accepting the email as is.");
    return Status::Accept;
  }

  if name.to_lowercase() == "content-type" {
    if let Some(email_data) = &mut context.data {
      debug!("Setting content type: {}", value);
      email_data.content_type = Some(value.to_string());
    }
    else {
      debug!("Initializing email data with content type: {}", value);
      context.data = Some(EmailData {
        body: vec![],
        content_type: Some(value.to_string()),
      })
    }
  }

  if name.to_lowercase() == "uwuify" {
    match value {
      "true" => {
        info!("Email has already been uwuified. Will not transform again.");
        return Status::Accept
      },
      "false" => {
        info!("Uwuify header is set to false. Bypassing transformation.");
        return Status::Accept
      },
      _ => {
        warn!("Uwuify header found but with unexpected value: {}. Ignoring. Will transform.", value);
        return Status::Continue
      }
    }
  }

  debug!("Header processing finished for - {}: {}. Continuing processing.", name, value);
  Status::Continue
}

async fn handle_body(context: &mut Context<EmailData>, chunk: Bytes) -> Status {
  debug!("Received a body chunk of size {} bytes.", chunk.len());

  if let Some(email_data) = &mut context.data {
    email_data.body.push(chunk)
  }
  else {
    debug!("No existing EmailData found. Initializing new data with the received chunk.");
    context.data = Some(EmailData {
      body: vec![chunk],
      content_type: None,
    })
  }

  Status::Continue
}

pub fn add_or_replace_uwuify_header(raw_bytes: &[u8], value: &str) -> Vec<u8> {
  // If the header already exists, replace its value; otherwise, append it.
  let uwuify_prefix = b"uwuify: ";
  let new_header = format!("uwuify: {}\r\n", value);

  // Check if the header exists
  if let Some(start_index) = raw_bytes.windows(uwuify_prefix.len()).position(|window| window.eq_ignore_ascii_case(uwuify_prefix)) {
    // Header exists. Replace its value.
    let end_index = raw_bytes[start_index..].iter().position(|&byte| byte == b'\n').unwrap_or(0) + start_index;
    let mut result = raw_bytes[..start_index].to_vec();
    result.extend_from_slice(new_header.as_bytes());
    result.extend_from_slice(&raw_bytes[end_index+1..]);
    result
  } else {
    // Header doesn't exist. Append it before the separating newline.

    // Find the position of the newline separating headers from body
    let separator = b"\r\n\r\n";
    let separator_pos = raw_bytes.windows(separator.len()).position(|window| window == separator);

    if let Some(index) = separator_pos {
      // Insert the header before this position
      let mut result = raw_bytes[..index].to_vec();
      result.extend_from_slice(new_header.as_bytes());
      result.extend_from_slice(&raw_bytes[index..]);
      result
    } else {
      // No separating newline found, which is unexpected.
      // Just appending for safety.
      let mut result = raw_bytes.to_vec();
      result.extend_from_slice(new_header.as_bytes());
      result
    }
  }
}

fn uwuify_parsed_mail(parsed_mail: &mut ParsedMail) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
  debug!("Uwuifying parsed mail with mimetype: {}", parsed_mail.ctype.mimetype);

  let new_line = b"\r\n".to_vec();
  let boundary_mark = b"--".to_vec();

  if parsed_mail.ctype.mimetype.starts_with("multipart/") {
    info!("Handling a multipart email.");

    let boundary = parsed_mail.ctype.params.get("boundary").ok_or_else(|| {
      error!("No boundary found in multipart email.");
      MailParseError::Generic("No boundary found")
    })?.as_bytes().to_vec();

    let mut uwuified_parts = vec![];
    for (index, subpart) in parsed_mail.subparts.iter_mut().enumerate() {
      debug!("Uwuifying part {} of the multipart email.", index + 1);
      let uwuified_part = uwuify_parsed_mail(subpart)?;
      uwuified_parts.push(uwuified_part);
    }

    let footer = [boundary_mark.clone(), boundary.clone(), boundary_mark.clone(), new_line.clone()].concat();
    let separator = [new_line.clone(), boundary_mark.clone(), boundary.clone(), new_line.clone()].concat();
    let uwuified_body = uwuified_parts.iter().fold(Vec::new(), |mut acc, part| {
      acc.extend_from_slice(part);
      acc.extend_from_slice(&separator);
      acc
    });

    let headers = parsed_mail.get_headers().get_raw_bytes().to_vec();
    let full_mail = [headers, new_line.clone(), uwuified_body, footer].concat();

    Ok(full_mail)
  } else {
    if let Some(uwufiy_header) = parsed_mail.get_headers().get_first_value("uwufiy") {
      match uwufiy_header.as_str() {
        "true" => {
          info!("Email already uwuified. Skipping transformation.");
          return Ok(parsed_mail.raw_bytes.to_vec());
        },
        "false" => {
          info!("Uwufiy transformation bypassed by header. Returning email as is.");
          return Ok(parsed_mail.raw_bytes.to_vec());
        },
        _ => {
          warn!("Invalid value for uwufiy header. Proceeding with transformation.");
        }
      }
    } else {
      info!("Uwufiy header not present. Proceeding with transformation.");
    }

    let body = &parsed_mail.get_body()?;
    debug!("Original body size: {}", ByteSize(body.len() as u64));
    let uwuified_body = if parsed_mail.ctype.mimetype == "text/html" {
      info!("Handling a text/html email.");
      uwuify_html(body)?
    } else {
      info!("Handling an email with mimetype: {}", parsed_mail.ctype.mimetype);
      uwuify(body)
    };
    debug!("Uwuified body size: {}", ByteSize(uwuified_body.len() as u64));

    let headers = parsed_mail.get_headers().get_raw_bytes().to_vec();
    let headers= add_or_replace_uwuify_header(&headers, "true");
    let full_mail = [headers, new_line, uwuified_body].concat();
    Ok(full_mail)
  }
}

fn uwuify_html(html: &str) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
  debug!("Starting the uwuify_html process.");

  let mut output = vec![];

  let mut rewriter = HtmlRewriter::new(
    Settings {
      element_content_handlers: vec![
        text!("*", |t| {
          let uwuified_bytes = uwuify(&t.as_str());
          let uwuified_text = String::from_utf8(uwuified_bytes)?;
          t.replace(uwuified_text.as_str(), ContentType::Text);
          Ok(())
        }),
      ],
      ..Settings::default()
    },
    |c: &[u8]| output.extend_from_slice(c),
  );

  rewriter.write(html.as_bytes())?;
  rewriter.end()?;

  Ok(output)
}

fn uwuify(text: &str) -> Vec<u8> {
  let mut temp1 = vec![0u8; round_up16(text.as_bytes().len()) * 16];
  let mut temp2 = vec![0u8; round_up16(text.as_bytes().len()) * 16];
  uwuify_sse(&text.as_bytes(), &mut temp1, &mut temp2).to_vec()
}

async fn handle_eom(context: &mut EomContext<EmailData>) -> Status {
  debug!("Handling end of message.");

  if let Some(email_data) = &mut context.data {
    let mut body: Vec<u8> = vec![];

    for chunk in &mut email_data.body {
      body = [body, chunk.to_vec()].concat();
    }

    let content_type = email_data.content_type.as_deref().unwrap_or("text/plain");
    let mail_header = format!("Content-Type: {}\r\n\r\n", content_type);
    let full_body = [mail_header.as_bytes().to_vec(), body].concat();

    // Using async block here
    let result: Result<(), Box<dyn std::error::Error>> = async {
      let mut mail = parse_mail(&full_body)?;
      let uwuified_body = uwuify_parsed_mail(&mut mail)?;
      context.actions.replace_body(&uwuified_body).await?;
      context.actions.add_header("uwuify", "true").await?;
      Ok(())
    }.await;  // We await the result of the async block immediately

    if let Err(e) = result {
      error!("Error processing end of message: {:?}", e);
      return Status::Accept;
    }
  } else {
    warn!("No email data available at end of message.");
  }

  debug!("Finished handling end of message.");
  Status::Continue
}
