import {
    resolve,
} from 'node:path/posix'

import Keyv from 'keyv'

export function getNyahCatEmailUserSqliteFilePath(): string {
    const cwd = process.cwd()
    return resolve(cwd, 'data', 'NyahCatEmailUser.sqlite')
}

export interface NyahCatEmailUser {
    discordId: string
    emails: Array<string>
    permittedEmails: number
}

const NyahCatEmailUserStore = new Keyv<NyahCatEmailUser>(`sqlite://${getNyahCatEmailUserSqliteFilePath()}`)

export async function storeNyahCatEmailUser(user: NyahCatEmailUser): Promise<void> {
    await NyahCatEmailUserStore.set(user.discordId, user)
}

export async function retrieveNyahCatEmailUser(id: NyahCatEmailUser['discordId']): Promise<NyahCatEmailUser> {
    let user = await NyahCatEmailUserStore.get(id)
    if (!user) {
        user = {
            discordId: id,
            emails: [],
            permittedEmails: 0,
        }
        await storeNyahCatEmailUser(user)
    }
    return user
}

export async function addEmailForNyahCatEmailUser(id: NyahCatEmailUser['discordId'], email: string): Promise<void> {
    const user = await retrieveNyahCatEmailUser(id)
    if (!user.emails.includes(email)) {
        user.emails.push(email)
        await storeNyahCatEmailUser(user)
    }
}

export async function removeEmailForNyahCatEmailUser(id: NyahCatEmailUser['discordId'], email: string): Promise<void> {
    const user = await retrieveNyahCatEmailUser(id)
    if (user.emails.includes(email)) {
        user.emails = user.emails.filter(userEmail => userEmail !== email)
        await storeNyahCatEmailUser(user)
    }
}

export async function updatePermittedEmailsForNyahCatEmailUser(id: NyahCatEmailUser['discordId'], permittedEmails: NyahCatEmailUser['permittedEmails']): Promise<void> {
    const user = await retrieveNyahCatEmailUser(id)
    user.permittedEmails = permittedEmails
    await storeNyahCatEmailUser(user)
}
