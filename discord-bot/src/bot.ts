import type {
  Interaction,
} from 'discord.js'

import {
  Client,
  Events,
} from 'discord.js'

import {
  getSetupScript,
  registerCommands,
  handleSlashCommand,
} from '@azemaviator/nyah-cat-discord-bot/api'

const discordToken = process.env['DISCORD_TOKEN']

console.log("Bot is starting...")

const discord = new Client({
  intents: []
})

discord.on(Events.ClientReady, async (client) => {
  if (!client.application) {
    return
  }

  await registerCommands(client)

  console.log(`${client.user.username} is online`)
})

discord.on(Events.InteractionCreate, async (interaction: Interaction) => {
  if (!interaction.isChatInputCommand()) {
    return
  }

  await handleSlashCommand(interaction)
})

async function run() {
  await getSetupScript()

  await discord.login(discordToken)
}

run()
  .catch(() => process.exit(1))
