import {
    runSetupCommand,
} from './execSetup'

export async function delEmail(email: string): Promise<number> {
    const {
        command,
        cleanUp,
    } = await runSetupCommand(['email', 'del', '-y', email])
    try {
        return await command
    }
    finally {
        cleanUp()
    }
}
