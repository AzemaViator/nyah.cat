import {
    runSetupCommand,
    streamToString,
} from './execSetup'

export async function listEmails(): Promise<Array<string>> {
    const {
        stdout,
        command,
        cleanUp,
    } = await runSetupCommand(['email', 'list'])
    try {
        if ((await command) === 0) {
            stdout.end()
            const rawOutput = await streamToString(stdout)
            const emails = rawOutput
                .split('\n')
                .filter(value => !!value)
                .map(emailLine => {
                    const match = emailLine.match(/\* (.*@nyah\.cat).*/)
                    if (match && (match.length ?? 0 > 1)) {
                        return match[1] ?? ''
                    }
                    return ''
                })
                .filter(value => !!value)
            return emails
        }
        else {
            return []
        }
    }
    finally {
        cleanUp()
    }
}

export async function stripDomain(emails: Array<string>): Promise<Array<string>> {
    return emails.map(email => email.replace(/@nyah\.cat/, ''))
}
