import {
    runSetupCommand,
} from './execSetup'

export async function updateEmailPassword(username: string, password: string): Promise<number> {
    const {
        command,
        cleanUp,
    } = await runSetupCommand(['email', 'update', username, password])
    try {
        return await command
    }
    finally {
        cleanUp()
    }
}
