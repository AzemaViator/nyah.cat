import type {
    Readable,
    Writable,
    Stream,
} from 'node:stream'

import {
    PassThrough,
} from 'node:stream'

import type {
    ChildProcess,
} from 'child_process'

import {
    resolve,
} from 'node:path/posix'

import {
    stat,
} from 'node:fs/promises'

import cuid from 'cuid'
import crossSpawn from 'cross-spawn'

const activeChildren = new Map<string, ChildProcess>()

function sigtermHandler() {
    for (const child of activeChildren.values()) {
        child.kill()
    }
}

process.on('SIGTERM', sigtermHandler)

export type RunOptions = {
    stdin?: Readable
    stdout?: Writable
    stderr?: Writable
}

export type RunReturn = {
    stdin: Readable
    stdout: Writable
    stderr: Writable
    command: Promise<number>
    id: string
    cleanUp: () => void
}

export async function runCommand(
    cmd: string,
    args: Array<string>,
    options?: RunOptions,
): Promise<RunReturn> {
    const stdoutPass = new PassThrough()
    const stderrPass = new PassThrough()
    const stdinPass = new PassThrough()

    const stdout = options?.stdout ?? stdoutPass
    const stderr = options?.stderr ?? stderrPass
    const stdin = options?.stdin ?? stdinPass
    const id = cuid()

    const child = crossSpawn(cmd, args, {stdio: ['pipe', 'pipe', 'pipe']})
    activeChildren.set(id, child)

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    stdin.pipe(child.stdin!)
    child.stdout?.pipe(stdout, { end: false })
    child.stderr?.pipe(stderr, { end: false })

    const command = new Promise<number>((resolve) => {
        child.on('error', (err: Error & { code: string }) => {
            activeChildren.delete(id)
            switch (err.code) {
                case 'ENOENT':
                    stderr.write(`command not found: ${cmd}\n`)
                    resolve(127)
                    break
                case 'EACCES':
                    stderr.write(`permission denied: ${cmd}\n`)
                    resolve(128)
                    break
                default:
                    stderr.write(`uncaught error: ${err.message}\n`)
                    resolve(1)
                    break
            }
        })
        child.on('exit', (code) => {
            activeChildren.delete(id)
            if (code !== null) {
                resolve(code)
            }
            else {
                resolve(129)
            }
        })
    })

    return {
        stdin,
        stdout,
        stderr,
        command,
        id,
        cleanUp: () => {
            stdoutPass.destroy()
            stderrPass.destroy()
            stdinPass.destroy()
        },
    }
}

export async function killCommand(id: string): Promise<void> {
    activeChildren.get(id)?.kill()
}

export async function streamToString(stream: Stream): Promise<string> {
    const chunks: Array<Buffer> = []
    return new Promise((resolve, reject) => {
        stream.on('data', (chunk) => chunks.push(Buffer.from(chunk)))
        stream.on('error', (err) => reject(err))
        stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
    })
}

export async function getSetupScript() {
    const cwd = process.cwd()
    const setupScriptPath = resolve(cwd, '..', 'setup.sh')
    try {
        await stat(setupScriptPath)
        return setupScriptPath
    }
    catch (e) {
        console.error('setup script not found!')
        throw e
    }
}

export async function runSetupCommand(args: Array<string>): Promise<RunReturn> {
    const cfd = await getSetupScript()
    return runCommand(cfd, args)
}
