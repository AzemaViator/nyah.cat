import {
    runSetupCommand,
} from './execSetup'

export async function addEmail(email: string, password: string): Promise<number> {
    const {
        command,
        cleanUp,
    } = await runSetupCommand(['email', 'add', email, password])
    try {
        return await command
    }
    finally {
        cleanUp()
    }
}
