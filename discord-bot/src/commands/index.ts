import type {
    ChatInputCommandInteraction,
    Client,
} from 'discord.js'

import type {
    SlashCommandMeta,
} from './SlashCommandMeta'

import {
    MailCommandMeta,
} from './mail'

const commands: Map<string, SlashCommandMeta> = new Map()
commands.set(MailCommandMeta.command.name, MailCommandMeta)

export async function registerCommands(client: Client<true>) {
    for (const commandMeta of commands.values()) {
        await client.application.commands.create(commandMeta.command)
    }
}

export async function handleSlashCommand(interaction: ChatInputCommandInteraction) {
    const commandName = interaction.commandName

    const commandMeta = commands.get(commandName)

    if (!commandMeta) {
        return
    }

    try {
        await commandMeta.handler(interaction)
    } catch (e) {
        console.error(e)
        await interaction.followUp({
            ephemeral: true,
            content: 'An error has occurred!',
        })
    }
}
