import type {
    CommandInteraction,
} from 'discord.js'

import type {
    SlashCommandMeta
} from '../SlashCommandMeta'

import {
    SlashCommandSubcommandBuilder,
    SlashCommandStringOption,
} from 'discord.js'

import {
    updateEmailPassword,
    retrieveNyahCatEmailUser,
} from '@azemaviator/nyah-cat-discord-bot/api'

export const ResetMailPasswordCommand = new SlashCommandSubcommandBuilder()
    .setName('reset')
    .setDescription('Reset the password for a mailbox you own')
    .addStringOption(
        new SlashCommandStringOption()
            .setName('username')
            .setRequired(true)
            .setDescription('The username you want. <username>@nyah.cat')
    )
    .addStringOption(
        new SlashCommandStringOption()
            .setName('password')
            .setRequired(true)
            .setDescription('The password to login with')
            .setMinLength(8)
    )

async function checkIfDiscordUserOwnsEmail(interaction: CommandInteraction, email: string) {
    const emailUser = await retrieveNyahCatEmailUser(interaction.user.id)
    return emailUser.emails.includes(email)
}

export async function ResetMailPasswordCommandHandler(interaction: CommandInteraction): Promise<void> {
    const usernameOption = interaction.options.get('username', true)
    const passwordOption = interaction.options.get('password', true)
    const username = usernameOption.value
    const password = passwordOption.value

    if (typeof username === 'string' && typeof password === 'string') {
        await interaction.deferReply({ephemeral: true})
        const email = `${username}@nyah.cat`

        if (!await checkIfDiscordUserOwnsEmail(interaction, email)) {
            await interaction.followUp({
                ephemeral: true,
                content: `${interaction.user.username} you do not own ${email} password reset denied.`
            })
            return
        }

        const exitCode = await updateEmailPassword(`${email}`, password)
        if (exitCode === 0) {
            await interaction.followUp({
                ephemeral: true,
                content: `The password for ${email} as been reset.`
            })
            return
        }
        else {
            await interaction.followUp({
                ephemeral: true,
                content: 'Server Side Error'
            })
            return
        }
    }

    return
}

export const ResetMailPasswordCommandMeta: SlashCommandMeta = {
    command: ResetMailPasswordCommand,
    handler: ResetMailPasswordCommandHandler,
}
