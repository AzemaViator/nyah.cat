import type {
    CommandInteraction,
} from 'discord.js'

import type {
    SlashCommandMeta
} from '../SlashCommandMeta'

import {
    SlashCommandSubcommandBuilder,
} from 'discord.js'

import {
    retrieveNyahCatEmailUser,
} from '@azemaviator/nyah-cat-discord-bot/api'

export const ListMailCommand = new SlashCommandSubcommandBuilder()
    .setName('list')
    .setDescription('List emails linked with your discord account.')

export async function ListMailCommandHandler(interaction: CommandInteraction): Promise<void> {
    await interaction.deferReply({ ephemeral: true })
    const user = await retrieveNyahCatEmailUser(interaction.user.id)

    if (user.emails.length) {
        await interaction.followUp({
            ephemeral: true,
            content: `The following emails are linked to your account:\n${user.emails.map(email => `${email}\n`)}`
        })
    }
    else {
        await interaction.followUp({
            ephemeral: true,
            content: `There are no emails linked with your account`,
        })
    }
}

export const ListMailCommandMeta: SlashCommandMeta = {
    command: ListMailCommand,
    handler: ListMailCommandHandler,
}
