import type {
    CommandInteraction,
} from 'discord.js'

import type {
    SlashCommandMeta,
} from '../SlashCommandMeta'

import {
    SlashCommandSubcommandBuilder,
    SlashCommandUserOption,
    SlashCommandNumberOption,
} from 'discord.js'

import {
    updatePermittedEmailsForNyahCatEmailUser,
    isBotAdmin,
} from '@azemaviator/nyah-cat-discord-bot/api'

export const MailAdminPermitCommand = new SlashCommandSubcommandBuilder()
    .setName('admin-permit')
    .setDescription('Set how many emails a user is allowed to create. (Requires Bot Admin)')
    .addUserOption(
        new SlashCommandUserOption()
            .setName('user')
            .setDescription('What user to set a permitted amount of emails for')
            .setRequired(true)
    )
    .addNumberOption(
        new SlashCommandNumberOption()
            .setName('max-allowed')
            .setDescription('The max amount of emails a user is allowed to make')
            .setMinValue(0)
            .setMaxValue(50)
            .setRequired(true)
    )

export async function MailAdminPermitCommandHandler(interaction: CommandInteraction): Promise<void> {
    if (await isBotAdmin(interaction.user)) {
        const discordUser = interaction.options.get('user', true).user
        const maxAllowedOption = interaction.options.get('max-allowed', true)
        const maxAllowed = maxAllowedOption.value

        if (discordUser && typeof maxAllowed === 'number') {
            await interaction.deferReply({ ephemeral: true })

            await updatePermittedEmailsForNyahCatEmailUser(discordUser.id, maxAllowed)

            await interaction.followUp({
                ephemeral: true,
                content: `${discordUser.username} permitted amount of emails has been set to ${maxAllowed}.`
            })
        }
    }
}

export const MailAdminPermitCommandMeta: SlashCommandMeta = {
    command: MailAdminPermitCommand,
    handler: MailAdminPermitCommandHandler,
}
