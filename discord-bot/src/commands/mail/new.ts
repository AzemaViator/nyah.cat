import type {
    CommandInteraction,
} from 'discord.js'

import type {
    SlashCommandMeta
} from '../SlashCommandMeta'

import {
    SlashCommandSubcommandBuilder,
    SlashCommandStringOption,
} from 'discord.js'

import {
    listEmails,
    addEmail,
    retrieveNyahCatEmailUser,
    addEmailForNyahCatEmailUser,
} from '@azemaviator/nyah-cat-discord-bot/api'

export const NewMailCommand = new SlashCommandSubcommandBuilder()
    .setName('new')
    .setDescription('Create a new mailbox')
    .addStringOption(
        new SlashCommandStringOption()
            .setName('username')
            .setRequired(true)
            .setDescription('The username you want. <username>@nyah.cat')
    )
    .addStringOption(
        new SlashCommandStringOption()
            .setName('password')
            .setRequired(true)
            .setDescription('The password to login with')
            .setMinLength(8)
    )

async function checkIfPermitted(interaction: CommandInteraction): Promise<boolean> {
    const emailUser = await retrieveNyahCatEmailUser(interaction.user.id)
    return emailUser.permittedEmails > emailUser.emails.length
}

async function checkIfUsernameIsValid(username: string): Promise<boolean> {
    return /^[A-Z0-9._%+-]+$/i.test(username.trim().toLowerCase())
}

export async function NewMailCommandHandler(interaction: CommandInteraction): Promise<void> {
    const usernameOption = interaction.options.get('username', true)
    const passwordOption = interaction.options.get('password', true)
    const username = usernameOption.value
    const password = passwordOption.value

    if (typeof username === 'string' && typeof password === 'string') {
        await interaction.deferReply({ ephemeral: true })
        const email = `${username.trim().toLowerCase()}@nyah.cat`

        if (!await checkIfPermitted(interaction)) {
            await interaction.followUp({
                ephemeral: true,
                content: `Sorry ${interaction.user.username} you are unable to add any new emails.`
            })
            return
        }

        if (!await checkIfUsernameIsValid(username)) {
            await interaction.followUp({
                ephemeral: true,
                content: `Sorry ${interaction.user.username} the username "${username}" is not valid.`
            })
            return
        }

        const existingEmails = await listEmails()
        if (existingEmails.includes(email)) {
            await interaction.followUp({
                ephemeral: true,
                content: 'email is already taken'
            })
            return
        }

        const exitCode = await addEmail(`${email}`, password)
        if (exitCode === 0) {
            await addEmailForNyahCatEmailUser(interaction.user.id, email)

            await interaction.followUp({
                ephemeral: true,
                content: `The email ${email} as been created for you.`
            })
            return
        }
        else {
            await interaction.followUp({
                ephemeral: true,
                content: 'Server Side Error'
            })
            return
        }
    }
    return
}

export const NewMailCommandMeta: SlashCommandMeta = {
    command: NewMailCommand,
    handler: NewMailCommandHandler,
}
