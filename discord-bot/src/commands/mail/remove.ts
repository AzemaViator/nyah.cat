import type {
    CommandInteraction,
    MessageActionRowComponentBuilder,
    ButtonInteraction,
} from 'discord.js'

import type {
    SlashCommandMeta,
} from '../SlashCommandMeta'

import {
    ActionRowBuilder,
    ButtonBuilder,
    ButtonStyle,
    ComponentType,
    SlashCommandStringOption,
    SlashCommandSubcommandBuilder,
    DiscordjsError,
    DiscordjsErrorCodes,
} from 'discord.js'

import cuid from 'cuid'

import {
    delEmail,
    removeEmailForNyahCatEmailUser,
    retrieveNyahCatEmailUser,
} from '@azemaviator/nyah-cat-discord-bot/api'

export const RemoveMailCommand = new SlashCommandSubcommandBuilder()
    .setName('remove')
    .setDescription('Remove a mailbox')
    .addStringOption(
        new SlashCommandStringOption()
            .setName('username')
            .setRequired(true)
            .setDescription('The username to remove. <username>@nyah.cat')
    )

async function checkIfDiscordUserOwnsEmail(interaction: CommandInteraction, email: string) {
    const emailUser = await retrieveNyahCatEmailUser(interaction.user.id)
    return emailUser.emails.includes(email)
}

export async function RemoveMailCommandHandler(interaction: CommandInteraction): Promise<void> {
    const usernameOption = interaction.options.get('username', true)
    const username = usernameOption.value

    if (typeof username === 'string') {
        await interaction.deferReply({ephemeral: true})
        const email = `${username.trim().toLowerCase()}@nyah.cat`

        if (!await checkIfDiscordUserOwnsEmail(interaction, email)) {
            await interaction.followUp({
                ephemeral: true,
                content: `You do not own ${email}.`
            })
            return
        }

        const confirmButtonId = cuid()
        const cancelButtonId = cuid()

        const actionRow = new ActionRowBuilder<MessageActionRowComponentBuilder>()
            .addComponents([
                new ButtonBuilder()
                    .setCustomId(confirmButtonId)
                    .setLabel('Confirm')
                    .setStyle(ButtonStyle.Success)
                ,
                new ButtonBuilder()
                    .setCustomId(cancelButtonId)
                    .setLabel('Cancel')
                    .setStyle(ButtonStyle.Danger)
                ,
            ])

        const confirmMessage = await interaction.followUp({
            ephemeral: true,
            content: `Please confirm removing ${email}, this is permanent and will delete all mail.`,
            components: [actionRow],
        })

        let buttonInteraction: ButtonInteraction

        try {
            buttonInteraction = await confirmMessage.awaitMessageComponent({
                time: 30000,
                componentType: ComponentType.Button,
            })
        }
        catch (e) {
            if (e instanceof DiscordjsError && e.code === DiscordjsErrorCodes.InteractionCollectorError) {
                await interaction.editReply({
                    content: 'Remove request timeout.',
                    components: [],
                })
                return
            }
            else {
                throw e
            }
        }

        if (buttonInteraction.customId === confirmButtonId) {
            const exitCode = await delEmail(`${email}`)
            if (exitCode === 0) {
                await removeEmailForNyahCatEmailUser(interaction.user.id, email)

                await buttonInteraction.update({
                    content: `The email ${email} as been removed.`,
                    components: [],
                })
                return
            } else {
                await buttonInteraction.update({
                    content: 'Server Side Error',
                    components: [],
                })
                return
            }
        } else if (buttonInteraction.customId === cancelButtonId) {
            await buttonInteraction.update({
                content: `The email ${email} has been spared a death.`,
                components: [],
            })
            return
        } else {
            await buttonInteraction.update({
                content: 'Unknown Button Interaction',
                components: [],
            })
        }
        return
    }
    else {
        return
    }
}

export const RemoveMailCommandMeta: SlashCommandMeta = {
    command: RemoveMailCommand,
    handler: RemoveMailCommandHandler,
}
