import type {
    CommandInteraction,
    MessageActionRowComponentBuilder,
    ButtonInteraction,
} from 'discord.js'

import type {
    SlashCommandMeta
} from '../SlashCommandMeta'

import {
    SlashCommandSubcommandBuilder,
    SlashCommandUserOption,
    SlashCommandStringOption,
    ActionRowBuilder,
    ButtonBuilder,
    ButtonStyle,
    ComponentType,
    DiscordjsError,
    DiscordjsErrorCodes,
} from 'discord.js'

import cuid from 'cuid'

import {
    listEmails,
    isBotAdmin,
    delEmail,
    removeEmailForNyahCatEmailUser,
} from '@azemaviator/nyah-cat-discord-bot/api'

export const MailAdminRemoveCommand = new SlashCommandSubcommandBuilder()
    .setName('admin-remove')
    .setDescription('Remove an email for a given user. (Requires Bot Admin)')
    .addUserOption(
        new SlashCommandUserOption()
            .setName('user')
            .setDescription('What user to set a permitted amount of emails for')
            .setRequired(true)
    )
    .addStringOption(
        new SlashCommandStringOption()
            .setName('username')
            .setRequired(true)
            .setDescription('The username to remove. <username>@nyah.cat')
    )

export async function MailAdminRemoveCommandHandler(interaction: CommandInteraction): Promise<void> {
    if (await isBotAdmin(interaction.user)) {
        const discordUser = interaction.options.get('user', true).user
        const usernameOption = interaction.options.get('username', true)
        const username = usernameOption.value

        if (discordUser && typeof username === 'string') {
            await interaction.deferReply({ ephemeral: true })
            const email = `${username.trim()}@nyah.cat`

            const existingEmails = await listEmails()

            if (existingEmails.includes(email)) {
                const confirmButtonId = cuid()
                const cancelButtonId = cuid()

                const actionRow = new ActionRowBuilder<MessageActionRowComponentBuilder>()
                    .addComponents([
                        new ButtonBuilder()
                            .setCustomId(confirmButtonId)
                            .setLabel('Confirm')
                            .setStyle(ButtonStyle.Success)
                        ,
                        new ButtonBuilder()
                            .setCustomId(cancelButtonId)
                            .setLabel('Cancel')
                            .setStyle(ButtonStyle.Danger)
                        ,
                    ])

                const confirmMessage = await interaction.followUp({
                    ephemeral: true,
                    content: `Please confirm removing ${email}, this is permanent and will delete all mail.`,
                    components: [actionRow],
                })

                let buttonInteraction: ButtonInteraction

                try {
                    buttonInteraction = await confirmMessage.awaitMessageComponent({
                        time: 30000,
                        componentType: ComponentType.Button,
                    })
                }
                catch (e) {
                    if (e instanceof DiscordjsError && e.code === DiscordjsErrorCodes.InteractionCollectorError) {
                        await interaction.editReply({
                            content: 'Remove request timeout.',
                            components: [],
                        })
                        return
                    }
                    else {
                        throw e
                    }
                }

                if (buttonInteraction.customId === confirmButtonId) {
                    const exitCode = await delEmail(`${email}`)
                    if (exitCode === 0) {
                        await removeEmailForNyahCatEmailUser(interaction.user.id, email)

                        await buttonInteraction.update({
                            content: `The email ${email} as been removed.`,
                            components: [],
                        })
                        return
                    } else {
                        await buttonInteraction.update({
                            content: 'Server Side Error',
                            components: [],
                        })
                        return
                    }
                } else if (buttonInteraction.customId === cancelButtonId) {
                    await buttonInteraction.update({
                        content: `The email ${email} has been spared a death.`,
                        components: [],
                    })
                    return
                } else {
                    await buttonInteraction.update({
                        content: 'Unknown Button Interaction',
                        components: [],
                    })
                    return
                }
            }
            else {
                await removeEmailForNyahCatEmailUser(discordUser.id, email)

                await interaction.followUp({
                    ephemeral: true,
                    content: `The email ${email} as been removed.`
                })
            }
        }
    }
}

export const MailAdminRemoveCommandMeta: SlashCommandMeta = {
    command: MailAdminRemoveCommand,
    handler: MailAdminRemoveCommandHandler,
}
