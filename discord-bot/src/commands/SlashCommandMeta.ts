import type {
    ChatInputCommandInteraction,
    SharedNameAndDescription,
} from 'discord.js'

export interface SlashCommandMeta {
    command: SharedNameAndDescription
    handler: (interaction: ChatInputCommandInteraction) => Promise<void>
}
