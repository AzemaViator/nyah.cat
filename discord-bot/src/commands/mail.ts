import type {
    ChatInputCommandInteraction,
} from 'discord.js'

import type {
    SlashCommandMeta,
} from './SlashCommandMeta'

import {
  SlashCommandBuilder,
} from 'discord.js'

import {
    NewMailCommandMeta,
    NewMailCommand,
} from './mail/new'

import {
    RemoveMailCommandMeta,
    RemoveMailCommand,
} from './mail/remove'

import {
    ResetMailPasswordCommandMeta,
    ResetMailPasswordCommand,
} from './mail/resetPassword'

import {
    ListMailCommandMeta,
    ListMailCommand,
} from './mail/list'

import {
    MailAdminPermitCommandMeta,
    MailAdminPermitCommand,
} from './mail/admin-permit'

import {
    MailAdminRemoveCommandMeta,
    MailAdminRemoveCommand,
} from './mail/admin-remove'

const subCommands: Map<string, SlashCommandMeta> = new Map()
subCommands.set(NewMailCommandMeta.command.name, NewMailCommandMeta)
subCommands.set(RemoveMailCommandMeta.command.name, RemoveMailCommandMeta)
subCommands.set(ResetMailPasswordCommandMeta.command.name, ResetMailPasswordCommandMeta)
subCommands.set(ListMailCommandMeta.command.name, ListMailCommandMeta)
subCommands.set(MailAdminPermitCommandMeta.command.name, MailAdminPermitCommandMeta)
subCommands.set(MailAdminRemoveCommandMeta.command.name, MailAdminRemoveCommandMeta)

export const MailCommand = new SlashCommandBuilder()
  .setName('mail')
  .setDescription('Mail Commands')
  .setDMPermission(false)
  .addSubcommand(NewMailCommand)
  .addSubcommand(RemoveMailCommand)
  .addSubcommand(ResetMailPasswordCommand)
  .addSubcommand(ListMailCommand)
  .addSubcommand(MailAdminPermitCommand)
  .addSubcommand(MailAdminRemoveCommand)

export async function MailCommandHandler(interaction: ChatInputCommandInteraction): Promise<void> {
    const subCommandName = interaction.options.getSubcommand(true)

    const subCommandMeta = subCommands.get(subCommandName)

    if (!subCommandMeta) {
        return
    }

    await subCommandMeta.handler(interaction)
}

export const MailCommandMeta: SlashCommandMeta = {
    command: MailCommand,
    handler: MailCommandHandler
}
