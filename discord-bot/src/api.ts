
import type {
    User,
} from 'discord.js'

export * from './mailserver'
export * from './storage'

export {
    registerCommands,
    handleSlashCommand,
} from './commands'

const BOT_ADMIN_IDS = [
    '1045248813218144256', // Azema
    '95808775193128960', // Patuit
]

export async function isBotAdmin(user: User): Promise<boolean> {
    return BOT_ADMIN_IDS.includes(user.id)
}
